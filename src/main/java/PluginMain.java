public class PluginMain {
    public static void main(String[] args) {

        PluginManager pluginManager = new PluginManager("src/main/java/pluginRootDirectory/pluginName/");
        Plugin pluginOne = pluginManager.load("plugin_1", "SomePlugin");
        pluginOne.doUsefull();

        Plugin pluginTwo = pluginManager.load("plugin_2", "SomePlugin");
        pluginTwo.doUsefull();

    }
}
