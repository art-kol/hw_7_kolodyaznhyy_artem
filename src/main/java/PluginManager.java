import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class PluginManager {

    private final String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }

    public Plugin load(String pluginName, String pluginClassName) {
        try {
            File pluginJarPath = new File(pluginRootDirectory+pluginName+".jar");
            URL pathJarByJar = pluginJarPath.toURI().toURL();
            URLClassLoader classLoader = new URLClassLoader(new URL[]{pathJarByJar});
            return (Plugin) classLoader.loadClass(pluginClassName).newInstance();

        } catch (MalformedURLException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }

        return null;

    }
}


